package internal

import (
	"fmt"
	"os"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/Pixdigit/Scriptoid/internal/AST"
	"gitlab.com/Pixdigit/Scriptoid/internal/compiler"
	"gitlab.com/Pixdigit/Scriptoid/parser"
	"llvm.org/llvm/bindings/go/llvm"
)

func PrintTokens(input antlr.CharStream) {

	lexer := parser.NewScriptoidLexer(input)

	for {
		t := lexer.NextToken()
		if t.GetTokenType() == antlr.TokenEOF {
			break
		}
		fmt.Printf("%s (%q)\n",
			lexer.SymbolicNames[t.GetTokenType()], t.GetText())
	}
}

func Test() {
	input, err := antlr.NewFileStream(os.Args[1])
	if err != nil {
		panic("Can not open file: " + fmt.Sprint(err))
	}
	//input_copy, _ := antlr.NewFileStream(os.Args[1])
	//PrintTokens(input_copy)

	lexer := parser.NewScriptoidLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)

	p := parser.NewScriptoidParser(stream)
	p.AddErrorListener(antlr.NewDiagnosticErrorListener(true))
	//p.BuildParseTrees = true

	visitor := NewScriptoidTreeVisitor(false)

	astTree := visitor.VisitScript(p.Script().(*parser.ScriptContext))

	printTree(astTree)
	fmt.Println()

	// astTree.SetParents(nil)
	err = astTree.ResolveRefs()
	if err != nil {
		panic(err)
	}
	show(astTree)

	printTree(astTree)
	fmt.Println()

	//_, _ := json.MarshalIndent(astTree, "", "\t")

	//fmt.Printf("%+v\n", astTree)

	module, err := compiler.Compile(astTree)
	if err != nil {
		panic(err)
	}
	module.Dump()
	engine, err := llvm.NewExecutionEngine(module)
	if err != nil {
		panic(fmt.Errorf("Error creating ExecutionEngine: %w", err))
	}
	engine.RunFunction(module.NamedFunction("main"), []llvm.GenericValue{})
}

func show(tr *AST.TransformNode) {
	for _, v := range tr.Variables() {
		dest, err := v.Transform.ResolvesTo()
		if err != nil {
			fmt.Printf("%v could not be resolved %v\n", v.Name(), err)
		} else {
			fmt.Printf("%v has destID %v\n", v.Name(), dest.ID())
		}
	}
	for _, t := range tr.Transforms() {
		show(t)
	}
}
