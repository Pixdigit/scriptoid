package compiler

import (
	"fmt"

	"gitlab.com/Pixdigit/Scriptoid/internal/AST"
	"llvm.org/llvm/bindings/go/llvm"
)

type compiler struct {
	ctx     llvm.Context
	module  llvm.Module
	builder llvm.Builder
}

func newCompiler(moduleName string) compiler {
	ctx := llvm.NewContext()
	return compiler{
		ctx:     ctx,
		module:  ctx.NewModule(moduleName),
		builder: llvm.NewBuilder(),
	}
}

func Compile(entry *AST.TransformNode) (llvm.Module, error) {
	comp := newCompiler(entry.Name())

	fnType := llvm.FunctionType(comp.ctx.VoidType(), []llvm.Type{}, false)
	llvm.AddFunction(comp.module, "main", fnType)
	block := llvm.AddBasicBlock(comp.module.NamedFunction("main"), "entry")
	comp.builder.SetInsertPoint(block, block.FirstInstruction())

	err := codeGen(entry, comp);	if err != nil {return comp.module, fmt.Errorf("Compile error: %w", err)}
	comp.builder.CreateRetVoid()

	if ok := llvm.VerifyModule(comp.module, llvm.ReturnStatusAction); ok != nil {
		return comp.module, fmt.Errorf("Can not verify compiled module: %w", ok)
	}

	return comp.module, nil
}
