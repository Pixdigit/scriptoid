package compiler

import "gitlab.com/Pixdigit/Scriptoid/internal/AST"

func getLib() *AST.TransformNode {
	var standardSpace *AST.ScopeNode = AST.NewScopeNode("main")
	var standardLib *AST.TransformNode = AST.NewTransformNode("std", []string{}, standardSpace)

	standardSpace.AddChild(AST.NewTransformNode("add", []string{}, AST.NewScopeNode("addBody")))

	return standardLib
}
