package compiler

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/Pixdigit/Scriptoid/internal/AST"
	"llvm.org/llvm/bindings/go/llvm"
)

func codeGen(entry AST.Unique, comp compiler) error {
	switch t := entry.(type) {
	case *AST.AtomicNode:
		var value llvm.Value
		switch t.Type {
		case AST.INT:
			value = llvm.ConstInt(comp.ctx.Int64Type(), uint64(t.Value.(int)), true)
		case AST.REAL:
			value = llvm.ConstFloat(comp.ctx.FloatType(), t.Value.(float64))
		default:
			return errors.New(fmt.Sprintf("Can not generate code for Atomic of type %v", t.Type))
		}
		pointer := comp.builder.CreateAlloca(value.Type(), t.Name())
		comp.builder.CreateStore(value, pointer)

	case *AST.TransformNode:
		for _, child := range t.Children() {
			err := codeGen(child, comp);	if err != nil {return fmt.Errorf("Could not generate code for child %v: %w", child.Name(), err)}
		}
	case *AST.ScopeNode:
		s, _ := json.Marshal(t.Children())
		return errors.New(fmt.Sprintf("Why is %v still here? Has %+v", t.Name(), string(s)))
	case *AST.ErrorNode:
		return errors.New("Compilation error: " + t.Info)
	default:
		return fmt.Errorf("Can not generate code for AST node of type %T", entry)
	}
	return nil
}
