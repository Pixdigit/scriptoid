package internal

import (
	"fmt"
	"strconv"

	"gitlab.com/Pixdigit/Scriptoid/internal/AST"
	"gitlab.com/Pixdigit/Scriptoid/parser"
)

type ScriptoidTreeVisitor struct {
	printParseInfo      bool
	printerIndentAmount int
}

func NewScriptoidTreeVisitor(printParseInfo bool) *ScriptoidTreeVisitor {
	return &ScriptoidTreeVisitor{
		printParseInfo,
		0,
	}
}

func (s *ScriptoidTreeVisitor) enter() {
	s.printerIndentAmount += 1
}
func (s *ScriptoidTreeVisitor) exit() {
	s.printerIndentAmount -= 1
}

func (s *ScriptoidTreeVisitor) printf(format string, arguments ...interface{}) {
	if s.printParseInfo {
		fmt.Print(indent(fmt.Sprintf(format, arguments...), s.printerIndentAmount, "\t") + "\n")
	}
}

func (s *ScriptoidTreeVisitor) VisitScript(ctx *parser.ScriptContext) *AST.TransformNode {
	s.printf("Visiting Script: %v", indent(ctx.GetText(), 1, "\t"))

	scope := s.VisitScope(ctx.Scope().(*parser.ScopeContext))
	//TODO: replace Script name by module or file name
	trans := AST.NewTransformNode("#script", []string{}, scope)
	trans.SetParents(nil)

	return trans
}

func (s *ScriptoidTreeVisitor) VisitScope(ctx *parser.ScopeContext) *AST.ScopeNode {
	lines := ctx.AllLine()
	s.printf("Visiting Scope: %v", indent(ctx.GetText(), 1, "\t"))
	s.printf("Scope has %v lines", len(lines))
	s.enter()
	defer s.exit()

	scope := AST.NewScopeNode("#anonymous")

	for _, line := range lines {
		lineCtx := line.(*parser.LineContext)

		//TODO: validate indents

		declaration := lineCtx.Declaration()
		if declaration != nil {
			node := s.VisitDeclaration(declaration.(*parser.DeclarationContext))
			err := scope.AddChild(node)
			if err != nil {
				scope.AddChild(AST.NewErrorNode(err.Error()))
			}
		}
	}

	return scope
}

func (s *ScriptoidTreeVisitor) VisitDeclaration(ctx *parser.DeclarationContext) AST.Unique {
	s.printf("Visiting Declaration: %v", ctx.GetText())
	s.enter()
	defer s.exit()

	var node AST.Unique

	//Mutually exclusive through parser definition
	function, _ := ctx.Function().(*parser.FunctionContext)
	variable, _ := ctx.Variable().(*parser.VariableContext)

	if function != nil {
		node = s.VisitFunction(function)
	} else if variable != nil {
		node = s.VisitVariable(variable)
	} else {
		node = AST.NewErrorNode(fmt.Sprintf("Declaration is neither function nor variable: %v", ctx.GetText()))
	}
	return node
}

func (s *ScriptoidTreeVisitor) VisitVariable(ctx *parser.VariableContext) AST.Unique {
	assignCtx := ctx.Assign().(*parser.AssignContext)
	s.printf("Visiting Variable \"%v\"", assignCtx.IDENTIFIER().GetSymbol().GetText())
	s.enter()
	defer s.exit()

	var_name := assignCtx.IDENTIFIER().GetText()
	literal := s.VisitLiteral(assignCtx.Literal().(*parser.LiteralContext))
	literal.SetName(var_name)

	return literal
}

func (s *ScriptoidTreeVisitor) VisitLiteral(ctx *parser.LiteralContext) AST.Unique {
	s.printf("Parsing literal \"%v\"", ctx.GetText())
	s.enter()
	defer s.exit()

	var node AST.Unique

	//Mutually exclusive
	integer := ctx.INT_LIT()
	float := ctx.FLOAT_LIT()
	str := ctx.STRING_LIT()
	fun := ctx.Function_literal()
	call := ctx.Call()
	block := ctx.Block()

	if integer != nil {
		i, err := strconv.Atoi(ctx.GetText());	if err != nil {return AST.NewErrorNode("Could not convert int literal to int value")}
		node = AST.NewAtomicNode("#literal", AST.INT, i)
	} else if float != nil {
		i, err := strconv.ParseFloat(ctx.GetText(), 64);	if err != nil {return AST.NewErrorNode("Could not convert float literal to REAL value")}
		node = AST.NewAtomicNode("#literal", AST.REAL, i)
	} else if str != nil {
		node = AST.NewErrorNode("Strings are not implemented yet")
	} else if fun != nil {
		fun := s.VisitFunction_literal(fun.(*parser.Function_literalContext))
		fun.Inline()
		node = fun
	} else if call != nil {
		node = s.VisitCall(call.(*parser.CallContext))
	} else if block != nil {
		scope := s.VisitBlock(block.(*parser.BlockContext))
		node = AST.NewIdentityNode(scope)
	} else {
		node = AST.NewErrorNode(fmt.Sprintf("Literal is not of a known type. Token: %v", ctx.GetText()))
	}
	return node
}

func (s *ScriptoidTreeVisitor) VisitFunction(ctx *parser.FunctionContext) *AST.TransformNode {
	s.printf("Visiting Function \"%v\"", ctx.IDENTIFIER().GetSymbol().GetText())
	s.enter()
	defer s.exit()

	func_name := ctx.IDENTIFIER().GetText()
	var parameters []string
	params := ctx.Parameters()
	if params != nil {
		parameters = s.VisitParameters(params.(*parser.ParametersContext))
	} else {
		parameters = []string{}
	}

	scope := s.VisitBlock(ctx.Block().(*parser.BlockContext))

	return AST.NewTransformNode(func_name, parameters, scope)
}

func (s *ScriptoidTreeVisitor) VisitBlock(ctx *parser.BlockContext) *AST.ScopeNode {
	return s.VisitScope(ctx.Scope().(*parser.ScopeContext))
}

func (s *ScriptoidTreeVisitor) VisitParameters(ctx *parser.ParametersContext) []string {
	params := ctx.AllIDENTIFIER()
	s.printf("Found %v Parameters", len(params))
	s.enter()
	defer s.exit()

	parameters := make([]string, len(params))
	for i, param := range params {
		parameters[i] = param.GetText()
	}

	return parameters
}

func (s *ScriptoidTreeVisitor) VisitFunction_literal(ctx *parser.Function_literalContext) *AST.TransformNode {
	var parameters []string
	params := ctx.Parameters()
	if params != nil {
		parameters = s.VisitParameters(params.(*parser.ParametersContext))
	} else {
		parameters = []string{}
	}
	scope := s.VisitBlock(ctx.Block().(*parser.BlockContext))
	return AST.NewImmediateTransformNode(parameters, scope)
}

func (s *ScriptoidTreeVisitor) VisitCall(ctx *parser.CallContext) *AST.CallNode {
	s.printf("Visiting call")
	s.enter()
	defer s.exit()

	var arguments AST.Arguments
	if args := ctx.Args(); args != nil {
		arguments = s.VisitArgs(args.(*parser.ArgsContext))
	}

	refCtx := ctx.Reference()
	fun := ctx.Function()
	funLit := ctx.Function_literal()

	var ref *AST.ReferenceNode
	if refCtx != nil {
		ref = AST.NewReferenceNode(refCtx.GetText())
	} else if fun != nil {
		transform := s.VisitFunction(fun.(*parser.FunctionContext))
		transform.Inline()
		ref = AST.NewLiteralReferenceNode(transform)
	} else if funLit != nil {
		trans := s.VisitFunction_literal(funLit.(*parser.Function_literalContext))
		ref = AST.NewLiteralReferenceNode(trans)
	} else {
		err := AST.NewErrorNode(fmt.Sprintf("Call has unknown function reference: %v", ctx.GetText()))
		ref = AST.NewResolvedReferenceNode("#error", err)
	}
	return AST.NewCallNode(arguments, ref, "#literal")
}

func (s *ScriptoidTreeVisitor) VisitArgs(ctx *parser.ArgsContext) AST.Arguments {
	args := AST.Arguments{}

	argsMap := ctx.GetArguments()
	for _, argMap := range argsMap {
		argMapCtx := argMap.(*parser.ArgMapContext)

		ident := argMapCtx.IDENTIFIER()
		ref := argMapCtx.Reference()
		lit := argMapCtx.Literal()

		var argName string
		var reference *AST.ReferenceNode
		if ident != nil {
			argName = ident.GetText()
			if ref != nil {
				refCtx := ref.(*parser.ReferenceContext)
				reference = AST.NewReferenceNode(refCtx.GetText())
			} else if lit != nil {
				literal := s.VisitLiteral(lit.(*parser.LiteralContext))
				switch t := literal.(type) {
				case *AST.TransformNode:
					reference = AST.NewLiteralReferenceNode(t)
				case *AST.AtomicNode:
					reference = AST.NewLiteralReferenceNode(t)
				case *AST.ErrorNode:
					err := AST.NewErrorNode(fmt.Sprintf("Could not parse literal for parameter %v : %v", argName, t.Error()))
					reference = AST.NewErrorReferenceNode(err)
				}
			} else {
				err := AST.NewErrorNode(fmt.Sprintf("Argument to call has unparsable reference: %v", argMapCtx.GetText()))
				reference = AST.NewErrorReferenceNode(err)
			}
		} else if ref != nil {
			refCtx := ref.(*parser.ReferenceContext)
			idents := refCtx.AllIDENTIFIER()

			argName = idents[len(idents)-1].GetText()
			reference = AST.NewReferenceNode(refCtx.GetText())
		} else {
			argName = "#unkown"
			err := AST.NewErrorNode(fmt.Sprintf("Argument to call could not be parsed: %v", argMapCtx.GetText()))
			reference = AST.NewErrorReferenceNode(err)
		}

		args[argName] = reference
	}

	return args
}
