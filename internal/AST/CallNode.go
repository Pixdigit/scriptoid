package AST

type Arguments map[string]*ReferenceNode

type CallNode struct {
	*Node
	Args      Arguments
	Transform *ReferenceNode
}

func NewCallNode(args Arguments, trans *ReferenceNode, returnName string) *CallNode {
	return &CallNode{
		NewNode(returnName),
		args,
		trans,
	}
}
