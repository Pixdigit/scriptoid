package AST

type AtomicType int

const (
	INT = AtomicType(iota)
	REAL
	STRING

	UNKNOWN
)

type AtomicNode struct {
	*Node
	Type  AtomicType
	Value interface{}
}

func NewAtomicNode(name string, constantType AtomicType, value interface{}) *AtomicNode {
	return &AtomicNode{
		NewNode(name),
		constantType,
		value,
	}
}
