package AST

import "fmt"

type ScopeNode struct {
	*Node
	variables  []*CallNode
	constants  []*AtomicNode
	transforms []*TransformNode
	errors     []*ErrorNode
}

func NewScopeNode(name string) *ScopeNode {
	return &ScopeNode{
		Node:       NewNode(name),
		variables:  []*CallNode{},
		constants:  []*AtomicNode{},
		transforms: []*TransformNode{},
		errors:     []*ErrorNode{},
	}
}

func (sn *ScopeNode) Children() []Unique {
	//Because children is a slice of interfaces we can not use append
	children := make([]Unique, len(sn.constants)+len(sn.variables)+len(sn.transforms)+len(sn.errors))
	i := 0
	for _, constant := range sn.constants {
		children[i] = constant
		i++
	}
	for _, variable := range sn.variables {
		children[i] = variable
		i++
	}
	for _, transform := range sn.transforms {
		children[i] = transform
		i++
	}
	for _, err := range sn.errors {
		children[i] = err
		i++
	}
	return children
}

func (sn *ScopeNode) AddChild(node Unique) error {
	for _, c := range sn.Children() {
		if c.Name() == node.Name() {
			return fmt.Errorf("Scope %v already has a child with name %v", sn.Name(), c.Name())
		}
	}

	switch t := node.(type) {
	case *CallNode:
		sn.variables = append(sn.variables, t)
	case *AtomicNode:
		sn.constants = append(sn.constants, t)
	case *TransformNode:
		sn.transforms = append(sn.transforms, t)
	case *ErrorNode:
		sn.errors = append(sn.errors, t)
	default:
		return fmt.Errorf("%T is not a type a scope can hold", node)
	}
	return nil
}

func (sn *ScopeNode) Variables() []*CallNode {
	vars := make([]*CallNode, len(sn.variables))
	copy(vars, sn.variables)
	return sn.variables
}
func (sn *ScopeNode) Constants() []*AtomicNode {
	consts := make([]*AtomicNode, len(sn.constants))
	copy(consts, sn.constants)
	return consts
}
func (sn *ScopeNode) Transforms() []*TransformNode {
	transforms := make([]*TransformNode, len(sn.transforms))
	copy(transforms, sn.transforms)
	return transforms
}
func (sn *ScopeNode) Errors() []*ErrorNode {
	errs := make([]*ErrorNode, len(sn.errors))
	copy(errs, sn.errors)
	return errs
}

func (sn *ScopeNode) Fold() error {
	// for _, variable := range sn.variables {
	// 	fmt.Println("Found Variable")
	// 	if !variable.Transform.IsResolved() {
	// 		fmt.Println("Variable is not Resolved")
	// 		break
	// 	}
	// 	fmt.Println("Variable is Resolved")
	//
	// 	trans, err := variable.Transform.ResolvesTo();	if err != nil {return fmt.Errorf("Could not fold transform for %v: %w", variable.Name(), err)}
	// 	fmt.Println("Found Trans")
	//
	// 	switch t := trans.(type) {
	// 	case *TransformNode:
	// 		t.TransformBlock.Fold()
	//
	// 		for _, child := range t.TransformBlock.Children() {
	// 			fmt.Println("NAME:" + child.Name())
	// 			child.SetName(trans.Name() + "." + child.Name())
	// 			if foldable, ok := child.(Foldable); ok {
	// 				foldable.Fold()
	// 				sn.AddChild(foldable)
	// 			} else {
	// 				sn.AddChild(child)
	// 			}
	// 		}
	//
	// 	case *AtomicNode:
	// 		//TODO
	// 	case *ErrorNode:
	// 		return fmt.Errorf("Found error while folding variable %v of %v: %v", trans.Name(), sn.Name(), t.Error())
	// 	}
	//
	// }
	// sn.transforms = []*TransformNode{}
	return fmt.Errorf("Not implemented")
}

func (sn ScopeNode) IsFolded() bool {
	isFolded := len(sn.variables) == 0
	for _, child := range sn.Children() {
		switch t := child.(type) {
		case Foldable:
			isFolded = isFolded && t.IsFolded()
		}
	}

	return isFolded
}
