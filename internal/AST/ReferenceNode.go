package AST

import (
	"fmt"
	"strings"
)

type ReferenceNode struct {
	ReferencePath string
	resolved      bool
	destination   Unique
}

func NewReferenceNode(reference string) *ReferenceNode {
	return &ReferenceNode{
		reference,
		false,
		nil,
	}
}

func NewResolvedReferenceNode(refName string, reference Unique) *ReferenceNode {
	return &ReferenceNode{
		refName,
		true,
		reference,
	}
}

func NewErrorReferenceNode(err *ErrorNode) *ReferenceNode {
	return NewResolvedReferenceNode("#error", err)
}

func NewLiteralReferenceNode(reference Unique) *ReferenceNode {
	return NewResolvedReferenceNode("#literal", reference)
}

func (rn *ReferenceNode) Path() []string {
	return strings.Split(rn.ReferencePath, ".")
}

func (rn *ReferenceNode) Root() string {
	//As the name can not be nil the path is never empty
	return rn.Path()[0]
}

func (rn *ReferenceNode) BaseName() string {
	return rn.Path()[len(rn.Path())-1]
}

func (rn *ReferenceNode) ChildReference() (*ReferenceNode, error) {
	if len(rn.Path()) < 2 {
		return nil, fmt.Errorf("Reference %v has no child", rn.ReferencePath)
	}
	return NewReferenceNode(strings.Join(rn.Path()[1:len(rn.Path())], ".")), nil
}

func (rn *ReferenceNode) Resolve(reference Unique) error {
	if rn.resolved {
		return fmt.Errorf("Reference to %v is already resolved", rn.ReferencePath)
	}

	rn.resolved = true
	rn.destination = reference

	return nil
}

func (rn *ReferenceNode) IsResolved() bool {
	return rn.resolved
}

func (rn *ReferenceNode) ResolvesTo() (Unique, error) {
	if !rn.resolved {
		return nil, fmt.Errorf("Reference %v is not resolved", rn.ReferencePath)
	} else {
		return rn.destination, nil
	}
}
