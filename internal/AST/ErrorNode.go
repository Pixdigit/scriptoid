package AST

type ErrorNode struct {
	*Node
	Info string
}

func NewErrorNode(info string) *ErrorNode {
	return &ErrorNode{
		Node: NewNode("#error"),
		Info: info,
	}
}

func (en *ErrorNode) Error() string {
	return en.Info
}
