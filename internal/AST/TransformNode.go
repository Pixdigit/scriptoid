package AST

import "fmt"

const anonName = "#anonymous"

type TransformNode struct {
	*ScopeNode
	Parameters []string
	parent     *ReferenceNode
	inlined    bool
	anonymous  bool
}

func NewTransformNode(name string, parameters []string, scope *ScopeNode) *TransformNode {
	scope.SetName(name)
	tn := &TransformNode{
		scope,
		parameters,
		NewReferenceNode("#parent"),
		false,
		false,
	}

	for _, childTrans := range tn.transforms {
		childTrans.SetParents(tn)
	}

	return tn
}

func NewInlineTransformNode(name string, parameters []string, scope *ScopeNode) *TransformNode {
	transform := NewTransformNode(name, parameters, scope)
	transform.inlined = true
	return transform
}

func NewImmediateTransformNode(parameters []string, scope *ScopeNode) *TransformNode {
	transform := NewInlineTransformNode(anonName, []string{}, scope)
	transform.anonymous = true
	return transform
}

func NewIdentityNode(scope *ScopeNode) *TransformNode {
	return NewImmediateTransformNode([]string{}, scope)
}

func (tn *TransformNode) AddChild(node Unique) error {
	if parentTrans, ok := node.(*TransformNode); ok {
		tn.SetParents(parentTrans)
	}
	return tn.ScopeNode.AddChild(node)
}

func (tn *TransformNode) Inlined() bool {
	return tn.inlined
}

func (tn *TransformNode) Inline() {
	tn.inlined = true
}

func (tn *TransformNode) resolveRef(ref *ReferenceNode) (*TransformNode, bool) {
	if len(ref.Path()) == 1 {
		if tn.Name() == ref.BaseName() {
			return tn, true
		}
		for _, childTrans := range tn.transforms {
			if childTrans.Name() == ref.BaseName() {
				return childTrans, true
			}
		}
		if tn.HasParent() {
			//tn only has parent if the reference is resolved.
			//As parent reference is only set by SetParents this is always a TransformNode
			return tn.parent.destination.(*TransformNode).resolveRef(ref)
		}

	} else {
		root, found := tn.findRoot(ref)
		if !found {
			return nil, found
		}

		for _, childTrans := range root.transforms {
			childRef, err := ref.ChildReference()
			if err != nil {
				panic(fmt.Sprintf("Reached unreachable statement. Maybe this helps: %v", err))
			}
			trans, found := childTrans.resolveRef(childRef)
			if found {
				return trans, found
			}
		}
	}
	return nil, false
}

func (tn *TransformNode) findRoot(ref *ReferenceNode) (trans *TransformNode, found bool) {
	trans = nil
	found = false

	for _, childTrans := range tn.transforms {
		if childTrans.Name() == ref.Root() {
			trans = childTrans
			found = true
			return
		}
	}

	if tn.HasParent() {
		//tn only has parent if the reference is resolved.
		//As parent reference is only set by SetParents this is always a TransformNode
		trans, found = tn.parent.destination.(*TransformNode).findRoot(ref)
	}
	return
}

func (tn *TransformNode) ResolveRefs() error {
	if !tn.ParentsSet() {
		return fmt.Errorf("%v can only resolve references when all children know their parents", tn.Name())
	}

	for _, childTrans := range tn.transforms {
		childTrans.ResolveRefs()
	}

	for _, variable := range tn.variables {
		transRef := variable.Transform
		if !transRef.IsResolved() {
			dest, found := tn.resolveRef(transRef)
			if found {
				transRef.Resolve(dest)
			}
		}
	}
	return nil
}

func (tn *TransformNode) Parent() (*TransformNode, error) {
	if tn.HasParent() && tn.ParentsSet() {
		return tn.parent.destination.(*TransformNode), nil
	} else {
		return nil, fmt.Errorf("Parent not set for node %v", tn.Name())
	}
}

func (tn *TransformNode) SetParents(parent *TransformNode) {
	if parent != nil {
		tn.parent.Resolve(parent)
	} else {
		tn.parent = nil
	}
	for _, child := range tn.transforms {
		child.SetParents(tn)
	}
}

func (tn *TransformNode) HasParent() bool {
	return tn.parent != nil && tn.parent.IsResolved()
}

func (tn *TransformNode) ParentsSet() bool {
	if tn.parent != nil && !tn.parent.IsResolved() {
		return false
	}
	for _, child := range tn.transforms {
		if !child.ParentsSet() {
			return false
		}
	}
	return true
}

func (tn *TransformNode) SetName(name string) {
	tn.ScopeNode.SetName(name)
	tn.anonymous = false
}
