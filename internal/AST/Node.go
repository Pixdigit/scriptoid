package AST

import "gitlab.com/Pixdigit/uniqueID"

type Unique interface {
	Name() string
	SetName(name string)
	ID() uniqueID.ID
}

type Foldable interface {
	Unique
	Fold()
	IsFolded() bool
}

type Node struct {
	id   uniqueID.ID
	name string
}

func NewNode(name string) *Node {
	return &Node{
		uniqueID.NewID(),
		name,
	}
}

func (n *Node) ID() uniqueID.ID {
	return n.id
}

func (n *Node) Name() string {
	return n.name
}

func (n *Node) SetName(name string) {
	n.name = name
}
