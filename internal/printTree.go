package internal

import (
	"fmt"
	"strings"

	"gitlab.com/Pixdigit/Scriptoid/internal/AST"
)

func printTree(root AST.Unique) {
	printIndented(0, root)
}

func printIndented(amount int, node AST.Unique) {
	fmt.Print(indent("", amount, "\t"))
	switch n := node.(type) {
	case *AST.AtomicNode:
		fmt.Printf("%v := %v (%v)\n", n.Name(), n.Value, n.Type)
	case *AST.TransformNode:
		fmt.Printf("Function %v(", n.Name())
		for i, param := range n.Parameters {
			if i > 0 {
				fmt.Print(", ")
			}
			fmt.Print(param)
		}
		fmt.Println("):")
		for _, child := range n.Children() {
			printIndented(amount+1, child)
		}
	case *AST.CallNode:
		argStrings := make([]string, len(n.Args))
		i := 0
		for param, arg := range n.Args {
			refName := arg.ReferencePath
			if dest, err := arg.ResolvesTo(); err == nil {
				if err, ok := dest.(*AST.ErrorNode); ok {
					refName = err.Error()
				}
			}
			argStrings[i] = fmt.Sprintf("%v: %v", param, refName)
			i++
		}
		argsString := strings.Join(argStrings, ", ")
		fmt.Printf("%v <- %v {%+v}\n", n.Name(), n.Transform.ReferencePath, argsString)
	case *AST.ErrorNode:
		fmt.Println("Error: ", n.Info)
	}
}

func indent(str string, amount int, indentStr string) string {
	lines := strings.Split(str, "\n")
	indentsStr := strings.Repeat(indentStr, amount)
	for i, line := range lines {
		lines[i] = indentsStr + line
	}
	return strings.Join(lines, "\n")
}
