#!/bin/zsh
rm ./parser/*.{go,interp,tokens}
java -Xmx500M -cp "/usr/local/lib/antlr-4.7.1-complete.jar:$CLASSPATH" org.antlr.v4.Tool -visitor -Dlanguage=Go -no-listener -o parser ScriptoidLexer.g4 &&
java -Xmx500M -cp "/usr/local/lib/antlr-4.7.1-complete.jar:$CLASSPATH" org.antlr.v4.Tool -visitor -Dlanguage=Go -no-listener -o parser ScriptoidParser.g4 &&
go run cmd/st/main.go test.sct
