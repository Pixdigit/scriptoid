# Scriptoid

Sorry for the unfinished and bare bones README but this project is still in its very early stages so half the time _I_ don't even know what I do.

### Getting started
This Project uses [ANTLRv4](https://www.antlr.org/) for lexing and parsing. Then using Go the parse tree is converted to an AST. The AST is then optimized and converted to a pure functional form. Finally and not yet even thought about is the compilation to LLVM IR.

So just install AntlrV4 and go and run `go get gitlab.com/Pixdigit/scriptoid` and you should have a running copy of the scriptoid compiler. However the testing script, which creates the parser and then parses the test file, is written using ZSH (probably Bash compatible). So if you are on windows, see how to use Antlr on windows, make sure to use Go as target language and enable visitors for antlr. If you struggle, feel free to ask me or make a bug report.

