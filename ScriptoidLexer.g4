lexer grammar ScriptoidLexer;


FUNC: 'func';
FUNC_LIT: '=>';
VAR: 'var';

DOT: '.';

L_PAREN: '(';
R_PAREN: ')';
L_CURLY: '{';
R_CURLY: '}';
ASSIGN: '=';
COMMA: ',';
COLON: ':';
SEMI: ';';

PLUS: '+';


IDENTIFIER: [A-Za-z] [A-Za-z0-9]* ('_' [A-Za-z0-9]+ )*;
INT_LIT: [1-9][0-9]*;
FLOAT_LIT: [1-9][0-9]*'.'[0-9]+;
STRING: '"' -> more, pushMode(STR);

NEWLINE: [\r\n];
ESCAPED_NEWLINE: '\\'[\r\n]([\t ]*)? -> channel(HIDDEN);

INDENT: '\t';
SPACE: ' ';
WS: SPACE;
COMMENT: '/*' .*? '*/' -> channel(HIDDEN);
LINE_COMMENT: '//' ~[\r\n]* -> channel(HIDDEN);

mode STR;
STRING_LIT: (~'"'| '\\"')*  '"' -> popMode;
