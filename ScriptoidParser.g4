parser grammar ScriptoidParser;

options {
	tokenVocab=ScriptoidLexer;
}

//(importDecl eos)*

script: scope;

block: '{' scope '}';
scope: (line)* closeIndents+=INDENT*;

line: INDENT* declaration eos
	| NEWLINE
	;

declaration:
	variable
//	| assign
	| function
	;


variable: VAR SPACE assign;
assign: IDENTIFIER SPACE ASSIGN SPACE literal;

function: FUNC SPACE IDENTIFIER SPACE? COLON SPACE (parameters SPACE)? block;
function_literal: '(' parameters? ')' SPACE FUNC_LIT SPACE block;
parameters: params+=IDENTIFIER (COMMA SPACE params+=IDENTIFIER)*;

literal:
	INT_LIT
	| FLOAT_LIT
	| STRING_LIT
	| function_literal
	| call
//	| reference
	| block
	;

reference: IDENTIFIER (DOT IDENTIFIER)*;

call: (reference | function | function_literal) '(' args? ')';
args: arguments+=argMap (COMMA SPACE arguments+=argMap)*;
argMap: (IDENTIFIER COLON SPACE (reference | literal)) | reference;

eos:
	(SEMI (SPACE | NEWLINE)) | NEWLINE
    | EOF
    ;
